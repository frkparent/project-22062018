<?php

namespace App\Controllers\Traits;

use WP_Query;

trait Posts {

    /**
     * Posts
     * Returns all the Posts
     *
     * @return void
     */
    public static function get( $param_args = [] )
    {
        // Default arguments for the WP_Query
        $args = [
            'post_type'      => 'post',
            'status'         => 'publish',
            'orderby'        => 'date',
            'order'          => 'DESC',
            'posts_per_page' => -1,
            'paged'          => 1,
            'cat'            => get_query_var('cat'),
            'tag'            => get_query_var('tag'),
            's'              => get_query_var('s'),
            'monthnum'       => get_query_var('monthnum'),
            'year'           => get_query_var('year'),
        ];

        // Merge the parameters with the current argument object
        if ( $param_args ) array_merge( $args, $param_args );

        // Pagination option
        if ( get_query_var('paged') ) $paged = get_query_var( 'paged' );
        if ( get_query_var('page') ) $paged = get_query_var( 'page' );

        // In the search page, force to query all kind of post types
        if ( isset($_POST['post_type']) ) {
            $args['post_type'] = $_POST['post_type'];
        } elseif ( is_search() ) {
            $args['post_type'] = ['module'];
        }

        // Number of posts to get
        // Check if a param was passed otherwise get the default option
        $posts_per_page = get_option('posts_per_page');
        if ( isset($_POST['posts_per_page']) ) {
            $args['posts_per_page'] = $_POST['posts_per_page'];
        }

        // The page to get the posts from
        if ( isset($_POST['page']) && $_POST['page'] != '' ) {
            $args['paged'] = $_POST['page'];
        }

        // Search query
        if ( isset($_POST['s']) && $_POST['s'] != '' ) {
            $args['s'] = $_POST['s'];
        }

        // The category for the posts
        if ( isset($_POST['cat']) && $_POST['cat'] != '' ) {
            $args['cat'] = $_POST['cat'];
        }

        // The date params (month or year)
        if ( isset($_POST['monthnum']) ) {
            $args['monthnum'] = $_POST['monthnum'];
        }
        if ( isset($_POST['year']) ) {
            $args['year'] = $_POST['year'];
        }

        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {
            return $query->posts;
        } else {
            echo '<p class="no-results">' . __('Nothing found for this query', 'theme') . '</p>';
        }
    }
}