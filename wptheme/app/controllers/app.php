<?php

namespace App;

use WP_Query;
use Sober\Controller\Controller;

class App extends Controller
{
    use Controllers\Traits\Posts;

    public function siteName()
    {
        return get_bloginfo('name');
    }


    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'theme');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'theme'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'theme');
        }
        return get_the_title();
    }


    public static function background()
    {
        $featured_img_id = get_post_thumbnail_id();
        $featured_img_array = wp_get_attachment_image_src( $featured_img_id, 'background', true );
        $bg_url = '';
        if ( $featured_img_array ) $bg_url = $featured_img_array[0];

        if ( get_field('background_image') ) {
            $bg = get_field('background_image');
            $bg_url = $bg['sizes']['background'];
        }

        return 'style="background-image: url(' . $bg_url . ');"';
    }


    /**
     * Get Posts
     * This function uses the "Posts" trait to create a WP_Query and return
     * the matching posts
     *
     * @param array $args
     * @return void
     */
    public static function get_posts( $args = [] ) {
        return Controllers\Traits\Posts::get( $args );
    }


    /**
     * Theme Options
     * Contains the ACF fields created in the Theme Options and that will be
     * used throughout the site
     *
     * @return object
     */
    public function options()
    {
        return (object) array(
            'copyrights' => get_field('copyrights', 'option'),
        );
    }


    /**
     * Social Media Links
     * Contains the ACF fields created in the Theme Options and that will be
     * used throughout the site
     *
     * @return object
     */
    public function social_links()
    {
        return (object) array(
            'twitter'   => get_field('twitter_url', 'option'),
            'pinterest' => get_field('pinterest_url', 'option'),
            'facebook'  => get_field('facebook_url', 'option'),
        );
    }


    /**
     * Show the landing overlay
     * This is an answer to @BUGFIX #12: the landing overlay needs to be
     * visible only when loading the site initially and NOT when the user
     * clicks on the main logo to go back.
     *
     * @return void
     */
    public function show_landing()
    {
        $is_visible = true;

        if ( isset($_GET['src']) && $_GET['src'] === 'origin' ) {
            $is_visible = false;
        }

        return $is_visible;
    }


    /**
     * Page Title
     * Contains the ACF fields for this section
     *
     * @return object
     */
    public function landing()
    {
        // The id of the landing page since we'll be retrieving ACF externally
        $LANDING_ID = 2;

        // The video poster
        $poster = get_field( 'video_poster', $LANDING_ID );

        return (object) array(
            'title'         => get_field( 'page_title', $LANDING_ID ),
            'description'   => get_field( 'page_description', $LANDING_ID ),
            'cta'           => get_field( 'cta_label', $LANDING_ID ),
            'video'         => get_field( 'background_video', $LANDING_ID ),
            'poster'        => $poster['sizes']['background'],
        );
    }


    /**
     * Navigation
     * Contains the ACF fields for the primary navigation
     *
     * @return object
     */
    public function nav()
    {
        // Get the primary navigation element by location
        $menu_name = 'primary_navigation';
        $locations = get_nav_menu_locations();
        $menu_id = $locations[ $menu_name ] ;
        $menu = wp_get_nav_menu_object($menu_id);

        // Setup data for the nexy module
        $next = get_field( 'next_module', $menu );

        // Setup data for the previous module
        $previous = get_field( 'previous_module', $menu );

        // Setup data for the recommended module
        $recommended = get_field( 'recommended_module', $menu );


        $menus = (object) array(
            'latest'         => get_field( 'latest_events', $menu ),
            'bottom_content' => get_field( 'bottom_content', $menu ),
        );

        if ( !empty($next->ID) ) {
            $menus->next  = [
                'id'        => $next->ID,
                'title'     => $next->post_title,
                'excerpt'   => get_the_excerpt( $next->ID ),
                'permalink' => get_the_permalink( $next->ID ),
                'image'     => get_field( 'preview_image', $next->ID ),
            ];
        }

        if ( !empty($previous->ID) ) {
            $menus->previous = [
                'id'        => $previous->ID,
                'title'     => $previous->post_title,
                'excerpt'   => get_the_excerpt( $previous->ID ),
                'permalink' => get_the_permalink( $previous->ID ),
                'image'     => get_field( 'preview_image', $previous->ID ),
            ];
        }

        if ( !empty($recommended->ID) ) {
            $menus->recommended = [
                'id'        => $recommended->ID,
                'title'     => $recommended->post_title,
                'excerpt'   => get_the_excerpt( $recommended->ID ),
                'permalink' => get_the_permalink( $recommended->ID ),
                'image'     => get_field( 'preview_image', $recommended->ID ),
            ];
        }

        return $menus;
    }


    /**
     * Page Builder
     * Contains the ACF fields for the content sections in this page
     *
     * @return object
     */
    public function slides()
    {
        $data = [];
        $pagebuilder_slides = get_field( 'content_slides' );

        if( $pagebuilder_slides ) {
            foreach( $pagebuilder_slides as $slide ) {

                // Section - Modules Carousel
                if ( $slide['acf_fc_layout'] === 'slide_modules' ) {
                    $this_obj = (object) [
                        'title'         => $slide['section_title'],
                        'description'   => $slide['section_description'],
                    ];
                }

                // Section - Module Description
                // This will be used inside the module inside pages
                if ( $slide['acf_fc_layout'] === 'section_module' ) {
                    $this_obj = (object) [
                        'title'         => $slide['section_title'],
                        'subtitle'      => $slide['section_subtitle'],
                        'content'       => $slide['section_content'],
                        'background'    => $slide['background_image']['sizes']['background'],
                        'show_sharing'  => $slide['show_sharing'],
                    ];
                }

                // Section - Video
                if ( $slide['acf_fc_layout'] === 'section_video' ) {
                    $this_obj = (object) [
                        'button'        => $slide['button_label'],
                        'title'         => $slide['video_title'],
                        'description'   => $slide['video_description'],
                        'video'         => $slide['video'],
                        'poster'        => $slide['video_poster']['sizes']['background'],
                    ];
                }

                // Section - General (light background)
                if ( $slide['acf_fc_layout'] === 'general_light' ) {
                    $this_obj = (object) [
                        'title'         => $slide['section_title'],
                        'subtitle'      => $slide['section_subtitle'],
                        'content'       => $slide['section_content'],
                        'background'    => $slide['background_image']['sizes']['background'],
                        'show_sharing'  => $slide['show_sharing'],
                    ];
                }

                // Section - General (dark background)
                if ( $slide['acf_fc_layout'] === 'general_dark' ) {
                    $this_obj = (object) [
                        'title'         => $slide['section_title'],
                        'subtitle'      => $slide['section_subtitle'],
                        'content'       => $slide['section_content'],
                        'gradient_color'=> [
                            'r' => $slide['gradient_color']['r'],
                            'g' => $slide['gradient_color']['g'],
                            'b' => $slide['gradient_color']['b'],
                        ],
                        'background'    => $slide['background_image']['sizes']['background'],
                        'show_sharing'  => $slide['show_sharing'],
                    ];
                }

                // Section - Hotspots
                if ( $slide['acf_fc_layout'] === 'section_hotspots' ) {
                    foreach( $slide['hotspots'] as $hotspot ) {
                        $hotspot['top'] >= '50' ? $ori = 'up' : $ori = 'down';

                        $hotspots[] = [
                            'title'         => $hotspot['title'],
                            'description'   => $hotspot['description'],
                            'top'           => $hotspot['top'],
                            'left'          => $hotspot['left'],
                            'orientation'   => $ori,
                        ];
                    }

                    $this_obj = (object) [
                        'background'    => $slide['section_background']['sizes']['background'],
                        'hotspots'      => $hotspots,
                    ];
                }

                // Section - Split Columns
                if ( $slide['acf_fc_layout'] === 'section_split' ) {
                    foreach( $slide['calls_to_action'] as $cta ) {
                        $ctas[] = [
                            'button_label'  => $cta['button_label'],
                            'button_url'    => $cta['button_url'],
                            'description'   => $cta['description'],
                        ];
                    }

                    $this_obj = (object) [
                        'left' => [
                            'title'      => $slide['left_title'],
                            'subtitle'   => $slide['left_subtitle'],
                            'content'    => $slide['left_content'],
                            'background' => $slide['background_image']['sizes']['background'],
                        ],
                        'right' => [
                            'next_module'           => $slide['next_module'],
                            'previous_module'       => $slide['previous_module'],
                            'recommended_module'    => $slide['recommended_module'],
                        ],
                        'ctas' => $ctas,
                    ];
                }

                $this_obj->layout = $slide['acf_fc_layout'];

                array_push( $data, $this_obj );
            }
        }

        return $data;
    }
}
