<?php

namespace Modules;

use WP_Query;
use Sober\Controller\Controller;

class Modules extends Controller
{
    public static function get_categories()
    {
        return get_terms('module_categories', [
            'hide_empty' => false,
        ]);
    }

    public static function get( $cat = null ) {
        $args = [
            'posts_per_page'    => -1,
            'post_type'         => 'module',
            'post_status'       => 'publish',
        ];

        if ( !is_null($cat) ) {
           $args['tax_query'] = [
                [
			        'taxonomy' => 'module_categories',
			        'field'    => 'term_id',
			        'terms'    => $cat,
                ],
            ];
        }

        $query = new WP_Query( $args );

        $modules = array_map(function ($post) {
            $preview = get_field( 'preview_image', $post->ID );
            $cats = wp_get_post_terms( $post->ID, 'module_categories' );

            return [
                'ID'        => $post->ID,
                'cat'       => $cats[0]->term_id,
                'title'     => $post->post_title,
                'permalink' => get_the_permalink( $post->ID ),
                'excerpt'   => wp_trim_words( $post->post_excerpt, 17 ),
                'image'     => $preview['sizes']['large'],
            ];
        }, $query->posts);

        return $modules;
    }


    public static function get_single( $id = 0 ) {
        $args = [
            'posts_per_page'    => 1,
            'post_type'         => 'module',
            'post_status'       => 'publish',
            'p'                 => $id,
        ];

        $query = new WP_Query( $args );

        // Get the first post returned
        $post = $query->posts[0];

        $preview = get_field( 'preview_image', $post->ID );
        $cats = wp_get_post_terms( $post->ID, 'module_categories' );

        $module = [
            'ID'        => $post->ID,
            'cat'       => $cats[0]->term_id,
            'title'     => $post->post_title,
            'permalink' => get_the_permalink( $post->ID ),
            'excerpt'   => wp_trim_words( $post->post_excerpt, 17 ),
            'image'     => $preview['sizes']['large'],
        ];

        return $module;
    }
}