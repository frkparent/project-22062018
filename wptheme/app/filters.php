<?php

namespace App;


/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Remove unnecessary classes
    $home_id_class = 'page-id-' . get_option('page_on_front');
    $remove_classes = array(
        'page-template-default',
        'page-template',
        $home_id_class
    );
    $classes = array_diff( $classes, $remove_classes );
    $classes = str_replace( 'template-template', 'template', $classes );

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});


/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});


/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);


/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});


/**
 * Set a custom excerpt length
 */
add_filter('excerpt_length', function ($length) {
    return 30;
});


/**
 * Add a custom template for the Search Results page
 */
add_filter('get_search_form', function () {
    $form = '';
    echo template('partials.searchform');
    return $form;
});


// Sanitize the filename of the uploaded files to remove accents and special
// characters
add_filter('sanitize_file_name', function ($filename) {
    return remove_accents( $filename );
});


/**
 * Allow upload of SVG in the media menu of WordPress
 * @return null
 */
add_filter( 'upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});


/*
* Callback function to filter the MCE settings
*/
add_filter( 'mce_buttons_2', function ($buttons) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
});
add_filter( 'tiny_mce_before_init', function ($init_array) {
    $style_formats = array(
    /*
    * Each array child is a format with it's own settings
    * Notice that each array has title, block, classes, and wrapper arguments
    * Title is the label which will be visible in Formats menu
    * Block defines whether it is a span, div, selector, or inline style
    * Classes allows you to define CSS classes
    * Wrapper whether or not to add a new block-level element around any selected elements
    */
        array(
            'title' => 'Primary Button style',
            'selector' => 'a,button',
            'classes' => 'button cta-primary',
        ),
    );

    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
});


/**
 * Cleanup the enqueued scripts to remove the "type" attribute that is now
 * unnecessary and triggers warning on W3C
 */
add_filter( 'script_loader_tag', function($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}, 10, 2);


/**
 * Remove the wrapper around the inputs generated automatically by Contact Form 7
 */
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});