import 'slick-carousel';


// Cache the selectors for the modules carousel
let $cat_links = $( '.modules-categories > .cat-link' );
let $moduleCarousel = $( '.modules-carousel' );
let $loader = $( '.modules-list > .loader' );


const filterModules = ( id = 0 ) => {
    // Hide the currently visible module previews
    // Unfilter the carousel to show all the slides again
    $moduleCarousel.find( '.slick-slide' ).fadeOut( 350 );

    // Show the loading animation
    setTimeout( function() {
        $loader.fadeIn( 150 );
    }, 350 );

    // Finally filter the slick carousel and show the result
    setTimeout( function() {
        $loader.fadeOut( 150, function() {
            if ( id !== '' ) {
                $moduleCarousel.find( '[data-cat="' + id + '"]' ).closest( '.slick-slide' ).fadeIn( 350 );
            } else {
                $moduleCarousel.find( '.slick-slide' ).fadeIn( 350 );
            }
        });
    }, 500 );
};


/**
 * The carousel in the "Section - Development Modules" section
 * The carousel will display 3 slides at the time and use the center mode
 * Clicking the "START" button in one of the module preview will slide the
 * mainCarousel to the maching module slide
 */
const modulesCarousel = () => {
    $( '.modules-carousel' ).slick({
        arrows: true,
        centerMode: true,
        slidesToShow: 5,
        rows: 0,
        swipe: false,
        responsive: [
        {
            breakpoint: 1450,
            settings: {
                slidesToShow: 3,
                centerMode: true,
            },
        },
        {
            breakpoint: 660,
            settings: {
                slidesToShow: 1,
                centerMode: true,
            },
        }],
    });

    // Bind the click event on the "START" button
    // @FEATURE: in version 2.0 of the site the general flow has changed;
    // instead of being one big slider the home only has the modules slider
    // and clicking one of them redirect to it's inside page inside of
    // sliding the main slider
    /*
    $( '[data-js="gotoslide"]' ).on( 'click', function(e) {
        let $this = $( this );
        let $target;
        let target_index = $this.closest( '.post-preview' ).data( 'target' );

        e.preventDefault();

        // Get the slide number for the target slide
        let target_slide_nb = $slides.index( $target );

        // If a "data-target" is present than use this one as target instead
        if ( $this.data('target') ) {
            target_index = $this.data( 'target' );
        }

        // Get matching slide object
        $target = $slides.filter( '[data-id="' + target_index + '"]' );

        // Move to the matching slide
        $main.slick( 'slickGoTo', target_slide_nb );
    });
    */

    // Bind the click event on the category links
    // This will filter the modules carousel to only show the previews matching
    // the selected category
    $cat_links.on( 'click', function(e) {
        e.preventDefault();

        let $this = $( this );

        // Get the selected category's ID
        let cat = $this.data( 'id' );

        // Highlight the newly selected category link
        $cat_links.removeClass( 'active' );
        $this.addClass( 'active' );

        // Filtyer the results
        filterModules( cat );
    });

    $( '.cat-list' ).on( 'change', function() {
        filterModules( $(this).val() );
    });
};


export default {
    initialize() {
        // Initialize the modules slider
        modulesCarousel();
    },
};