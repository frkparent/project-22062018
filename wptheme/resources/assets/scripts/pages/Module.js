import 'slick-carousel';


// Cache the selectors for the main carousel and it's slides
const $main = $( '.main' );
let $slides = $main.find( '> .section' );

/**
 * The site will be one big slider with every sections as slides
 * This is the slider with all the child of the "<main>" tag
 */
const mainCarousel = () => {
    $main.on( 'beforeChange', function(event, slick, currentSlide, nextSlide) {
        // Get the newly loaded slide
        let $newSlide = $slides.eq( nextSlide );

        // This is the edge case of the edge cases:
        // When sliding to a section "General" with a dark background, on
        // mobile only, we need to hide the gradient on the ":after" of the
        // <main> tag since it is white and would clash with the black
        // background of this slide
        if ( $newSlide.hasClass('section-general') &&
             $newSlide.hasClass('general_dark') &&
             window.innerWidth <= 660 ) {

            $main.addClass( 'hide-gradient' );
        } else {
            $main.removeClass( 'hide-gradient' );
        }
    }).on( 'afterChange', function(event, slick, currentSlide) {
        // Get the newly loaded slide
        let $newSlide = $slides.eq( currentSlide );

        // If the new slide is the "Hotspot" one, add a class to start
        // the animation on the hotspot svg
        if ( $newSlide.hasClass('section-hotspots') ) {
            setTimeout( function() {
                $newSlide.addClass( 'loaded' );
            }, 200);
        }
    }).slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        easing: 'cubic-bezier(0.77, 0, 0.175, 1)',
        speed: 600,
        slide: '.section',
        rows: 0,
        responsive: [
        {
            breakpoint: 660,
            settings: {
                arrows: false,
            },
        }],
    });
};


export default {
    initialize() {
        // Initialize the main slider
        mainCarousel();
    },
};