// Page-specific scripts to initialize
import page_home from './pages/Home';
import page_module from './pages/Module';

// Load Events
$(() => {
    if ( document.body.classList.contains('home') ) {
        page_home.initialize();
    }

    if ( document.body.classList.contains('single-module') ) {
        page_module.initialize();
    }
});