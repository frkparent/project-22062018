/* ========================================================================
 * VELUX
 * ======================================================================== */

// Import components
import './components/fontawesome';
import './components/nav';
import './components/search';
import './components/video';
import './components/sharing';
import './components/details';

// Initialize the "Hotspots" slide.
import './components/hotspots';

// Scripts for the landing overlay
import './components/landing';

// Router
// Used to dynamically initialized Views based on the current page
import './router';