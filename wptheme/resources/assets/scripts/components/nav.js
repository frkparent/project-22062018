$(() => {
    // Cache the mobile navigation button
    let $mobileNavBtn = $( '.burger' );

    // Cache the module link in the navigation
    let $navModuleLinks = $( '.nav-main' ).find( '[data-js="toggle-module"]' );

    // Toggle the navigation
    $mobileNavBtn.on( 'click', function( e ) {
        e.preventDefault();
        document.body.classList.toggle( 'mobile-nav-opened' );
    });

    // When clicking one of the navigation menu ("Previous Module",
    // "Recommended Module", "Latest Events"):
    // 1- Close the Landing overlay if it's still there;
    // 2- Move the carousel to the matching module
    // 3- Close the navigation overlay;
    //
    // [UPDATE 25-06-2018]
    // We realized that the client had something different in mind for the
    // navigation; instead of sliding to the matching slide when a link is
    // clicked, simply toggle the accordion under it and reveal the module's
    // content.
    // The following is then deprecated but we'll keep in case they change
    // their mind in the future.
    /*
    $( '.nav-main' ).find( '.menu-link' ).on( 'click', function(e) {
        // Check if the clicked menu as a module ID;
        // if so we'll need to move to the matching slide and otherwise
        // leave the default behaviour
        let $this = $( this );
        let id = $this.data( 'module-id' );

        // Cache all the main slide. Can't do that earlier because they can
        // change dynamically
        let $slides = $main.find( '.section' );
        let $target;
        let targetIndex = 0;

        // Delay before moving the carousel.
        // This is useful if we need to hide the landing overlay before moving
        let delay = 0;

        // 1- Close the overlay landing
        if ( $('#page-landing').length ) {
            Landing.close();
            delay = 600;
        }

        if ( id ) {
            e.preventDefault();
            $target = $slides.filter( '[data-id="' + id + '"]' );
        }

        // If matching slide is found, move the carousel
        if ( $target.length ) {
            targetIndex = $slides.index( $target );

            setTimeout( function() {
                $main.slick( 'slickGoTo', targetIndex );
            }, delay);

            // Close the overlay after a short delay it takes to move the carousel
            setTimeout( function() {
                document.body.classList.remove( 'mobile-nav-opened' );
            }, delay * 2);

            // Reset the delay
            delay = 0;
        }
    });
    */


    // **NEW** behaviour for the menu links in the sidebar navigation
    // When clicked, toggle the module accordion underneath and close the others
    $navModuleLinks.on( 'click', function(e) {
        let $target = $( e.target );
        let $target_parent = $target.parent();

        e.preventDefault();

        // Close the other accordions...
        $navModuleLinks.not( e.target ).parent().removeClass( 'active' );

        // ... and then open the selected one
        if ( $target_parent.hasClass('active') ) {
            $target_parent.removeClass( 'active' );
        } else {
            $target_parent.addClass( 'active' );
        }
    });


    // Bind an event on keyup to detect the Escape key.
    // If pressed while the navigation menu is opened, close it
    $( document ).on( 'keyup', function( e ) {
        if ( e.keyCode === 27 && document.body.classList.contains('mobile-nav-opened') ) {
            $mobileNavBtn.trigger( 'click' );
        }
    });

    // When the user is resizing the window, prevent the main navigation
    let resizeTimer;
    $( window ).on('resize', function() {
        document.body.classList.add( 'resizing' );
        clearTimeout( resizeTimer );
        resizeTimer = setTimeout(function() {
            document.body.classList.remove( 'resizing' );
        }, 250);
    });
});