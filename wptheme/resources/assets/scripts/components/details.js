if ( !Modernizr.details ) {
    $( 'details' )
        .find( '*:not(summary)' ).hide().end()
        .find( 'summary' ).prepend( '<i>+ </i>' )
        .on( 'click', function() {
            const $this = $( this );
            const $icon = $this.find( 'i' );

            $this.toggleClass( 'opened' ).siblings().toggle();
            $this.hasClass('opened') ? $icon.text('- ') : $icon.text('+ ');
        });
}