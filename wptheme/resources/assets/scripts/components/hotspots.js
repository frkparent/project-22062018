/**
 * When clicking one of the hotspot button [+] we toggle the descriptions
 */
$(() => {
    // let event = 'mouseenter mouseleave';
    // @BUGFIX #4: Client asked that the hotspot descriptions show on click
    // instead of mouseover like the example website
    let event = 'click';

    if ( window.innerWidth <= 1024 ) {
        event = 'click';
    }

    $( '[data-js="toggle-hotspot"]' ).on( event, function() {
        $( this ).closest( '.hotspot' ).toggleClass( 'active' );
    });

    /**
     *
     */
    $( '.hotspots-list' ).find( 'dd' ).on( 'click', function() {
        $( this ).parent().toggleClass( 'opened' );
    });
});