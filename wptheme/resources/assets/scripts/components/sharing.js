/**
 * When clicking the sharing icon toggle the social sharing options
 */
$(() => {
    $( '[data-js="toggle-social"]' ).on( 'click', function() {
        $( this ).parent().toggleClass( 'active' );
    });
});