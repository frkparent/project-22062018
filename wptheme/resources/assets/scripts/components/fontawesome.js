// FontAwesome
// Explicit import for the icons used in the project. If more of them are
// required the library feature could be used instead
// https://fontawesome.com/how-to-use/use-with-node-js#pro
import fontawesome from '@fortawesome/fontawesome';

// Import the icons...
import faTwitter from '@fortawesome/fontawesome-free-brands/faTwitter';
import faPinterest from '@fortawesome/fontawesome-free-brands/faPinterest';
import faFacebook from '@fortawesome/fontawesome-free-brands/faFacebookF';
import faShareAlt from '@fortawesome/fontawesome-pro-solid/faShareAlt';

// ... and then add them to the library
fontawesome.library.add(
    faTwitter,
    faFacebook,
    faPinterest,
    faShareAlt
);