/**
 * When clicking the "Play Video" button on the video section:
 * 1- Fade out the video title and description
 * 2- Fade out the poster
 * 3- Fade in the video and start playing it
 */
const play = ( $el ) => {
    let $this = $el.closest( '.section' );
    let $player = $this.find( 'iframe' );
    let iframe_url = $player[0].src;

    // Show the iframe
    $player.show();

    // Fadeout the video title and description
    $this.addClass( 'unload' );

    // Add the autoplay param in the ifram URL to auto start it
    if ( iframe_url && iframe_url.indexOf('?') > 1 ) {
        $player[0].src += '&autoplay=1';
    } else {
        $player[0].src += '?autoplay=1';
    }
};

$(() => {

    $( '.video-overlay, [data-js="play"]' ).on( 'click', function() {
        play( $(this) );
    });
});