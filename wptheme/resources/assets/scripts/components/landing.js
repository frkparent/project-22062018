const main = document.querySelector( 'main' );
const landing = document.getElementById( 'page-landing' );


const close = () => {
    landing.classList.add( 'unload' );
    main.classList.add( 'active' );

    // Remove the overlay from the DOM after a second
    setTimeout( function() {
        $( landing ).remove();
    }, 1000);

    // Fade in the search form and social links visible on the module slide
    $( '.site-header' ).find( '.section-search' ).fadeIn( 350 );
};

const initialize = () => {
    /**
     * When clicking the "Enter" button on the overlay, 2 things happen:
     * 1- We hide the overlay using CSS transition and then show the main content;
     * 2- We update the URL to the new page to ease the navigation
     */
    $( '[data-js="hide-landing"]' ).on( 'click', function() {
        close();
    });
};


// Initialize the landing overlay once the DOM is ready
$(() => {
    initialize();
});


// Export the close function to be used in the navigation module
export { close };