$(() => {
    const $body = $( document.body );

    /**
     * Initialize the global search throughout the site.
     * When the user clicks the 'Rechercher' links, a full width search container
     * slides down from the top with the search input
     */
    $( '.lnk-search' ).on( 'click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $body.toggleClass( 'search-visible' );
        $( '.search-form' ).find( '.search-field' ).focus();

        $body.on( 'click.menuHide', function() {
            $body
                .toggleClass( 'search-visible' )
                .off( 'click.menuHide' );
        });
    });
    $( '.search-form' ).on( 'click', function(e) {
        e.stopPropagation();
    });
});