<article class="post-preview" data-target="{{ $module['ID'] }}" data-cat="{{ $module['cat'] }}">
    <a class="post-image" style="background-image: url({!! $module['image'] !!});" href="{!! $module['permalink'] !!}">
        <span class="button cta-primary btn-start">
            {{ __('Start', 'theme') }}
        </span>
    </a>

    <a class="post-content" href="{!! $module['permalink'] !!}">
        <h3 class="post-title">{{ $module['title'] }}</h3>

        {!! $module['excerpt'] !!}
    </a>
</article>