@extends('layouts.app')

@section('content')
    @include('partials.page-header')

    @if (!have_posts())
        {{ __('Sorry, no results were found.', 'sage') }}
    @endif

    @while (have_posts()) @php(the_post())
        @include('partials.content-'.get_post_type())
    @endwhile

    {!! get_the_posts_navigation() !!}
@endsection
