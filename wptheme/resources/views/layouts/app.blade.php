<!doctype html>
<html @php(language_attributes())>
    @include('partials.head')

    <body @php(body_class())>
        <!--[if lt IE 9]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'theme'); ?>
            </div>
        <![endif]-->

        @php(do_action('get_header'))
        @include('partials.header')

        @include('partials.nav-panel')

        @if( $show_landing )
            @include('partials.content-landing')
        @endif

        <div id="document" role="document">
            <main class="main {{ !$show_landing ? 'active' : '' }}">
                @yield('content')
            </main>
        </div>

        @php(do_action('get_footer'))

        @php(wp_footer())
  </body>
</html>
