<div id="page-landing">
    <div class="video-wrapper">
        <video muted autoPlay loop src="{!! $landing->video !!}" poster="{!! $landing->poster !!}">
            {{ __('Sorry, your browser doesn\'t support embedded videos.', 'theme') }}
        </video>
    </div>

    <div class="container">
        <div class="content">
            @if( !empty($landing->title) )
                <h1 class="page-title">{!! $landing->title !!}</h1>
            @endif

            @if( !empty($landing->description) )
                <p class="page-description">{!! $landing->description !!}</p>
            @endif

            @if( !empty($landing->cta) )
                <button class="button cta-primary" data-js="hide-landing" type="button">
                    {{ $landing->cta }}
                </button>
            @endif
        </div>
    </div>
</div>