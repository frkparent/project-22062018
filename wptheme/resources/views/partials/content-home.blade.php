@foreach( $slides as $slide )

    {{--
      -- If the slide "Section - Development Modules" is inserted in the page,
      -- right after it we'll query the "module" post type and automatically insert
      -- each of them as individual slides.
      --}}
    @if( $slide->layout === 'slide_modules' )
        <section class="section section-modules">
            <div class="section--inner">
                <header class="section-header">
                    <h2 class="section-title">{!! $slide->title !!}</h2>

                    @if( !empty($slide->description) )
                        <div class="section-description">{!! $slide->description !!}</div>
                    @endif
                </header>

                <div class="section-content">
                    <div class="modules-categories desktop-only">
                        <a class="cat-link active" href="#all" data-id="">{{ __('All', 'theme') }}</a>

                        @foreach( Modules::get_categories() as $cat )
                            <a class="cat-link" href="#{{ $cat->slug }}" data-id="{{ $cat->term_id }}">
                                {{ $cat->name }}
                            </a>
                        @endforeach
                    </div>

                    <div class="modules-categories mobile-only">
                        <select class="cat-list">
                            <option>{{ __('All', 'theme') }}</option>

                            @foreach( Modules::get_categories() as $cat )
                                <option class="cat-link" data-id="{{ $cat->term_id }}" value="{{ $cat->term_id }}">
                                    {{ $cat->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="modules-list">
                        @include('ui.loader')

                        <div class="modules-carousel">
                            @foreach( Modules::get() as $module )
                                @include('blocks.module-preview', $module)
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endforeach