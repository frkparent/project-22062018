<div class="nav-panel">
    <button class="burger" type="button">
        <span></span>
        <span></span>
        <span></span>
    </button>

    {{ get_search_form() }}

    <nav class="nav-main">
        <div class="menu-title">{{ __('Main menu', 'theme') }}</div>

        @if( !empty($nav->next) )
            <div class="menu-link" data-module-id="{{ $nav->next['id'] }}">
                <div data-js="toggle-module">{{ __('Next Module', 'theme') }}</div>

                <a class="menu-module" href="{{ $nav->next['permalink'] }}">
                    <div class="menu-module--inner">
                        <img class="featured-image desktop-only" src="{!! $nav->next['image']['sizes']['medium'] !!}" alt="" />
                        <h5 class="module-title">{{ $nav->next['title'] }}</h5>

                        {{ $nav->next['excerpt'] }}
                    </div>
                </a>
            </div>
        @endif

        @if( !empty($nav->previous) )
            <div class="menu-link" data-module-id="{{ $nav->previous['id'] }}">
                <div data-js="toggle-module">{{ __('Previous Module', 'theme') }}</div>

                <a class="menu-module" href="{{ $nav->previous['permalink'] }}">
                    <div class="menu-module--inner">
                        <img class="featured-image desktop-only" src="{!! $nav->previous['image']['sizes']['medium'] !!}" alt="" />
                        <h5 class="module-title">{{ $nav->previous['title'] }}</h5>

                        {{ $nav->previous['excerpt'] }}
                    </div>
                </a>
            </div>
        @endif

        @if( !empty($nav->recommended) )
            <div class="menu-link" data-module-id="{{ $nav->recommended['id'] }}">
                <div data-js="toggle-module">{{ __('Recommended Module', 'theme') }}</div>

                <a class="menu-module" href="{{ $nav->recommended['permalink'] }}">
                    <div class="menu-module--inner">
                        <img class="featured-image desktop-only" src="{!! $nav->recommended['image']['sizes']['medium'] !!}" alt="" />
                        <h5 class="module-title">{{ $nav->recommended['title'] }}</h5>

                        {{ $nav->recommended['excerpt'] }}
                    </div>
                </a>
            </div>
        @endif

        @if( $nav->latest )
            <a class="menu-link" href="{!! $nav->latest !!}">{{ __('Latest Events', 'theme') }}</a>
        @endif
    </nav>

    <div class="social-links">
        @if( !empty($social_links->twitter) )
        <a class="social-link" href="{!! $social_links->twitter !!}" target="_blank" rel="noopener">
            <i class="fab fa-twitter"></i>
        </a>
        @endif

        @if( !empty($social_links->pinterest) )
        <a class="social-link" href="{!! $social_links->pinterest !!}" target="_blank" rel="noopener">
            <i class="fab fa-pinterest"></i>
        </a>
        @endif

        @if( !empty($social_links->facebook) )
        <a class="social-link" href="{!! $social_links->facebook !!}" target="_blank" rel="noopener">
            <i class="fab fa-facebook-f"></i>
        </a>
        @endif
    </div>

    @if( $nav->bottom_content )
        <div class="bottom-content">{!! $nav->bottom_content !!}</div>
    @endif
</div>