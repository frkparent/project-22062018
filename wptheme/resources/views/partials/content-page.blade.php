<div class="container content">
    <div class="container--inner">
        @php(the_content())
    </div>
</div>