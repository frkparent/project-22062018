<div class="container">
    <div class="container--inner">
        {{ get_search_form() }}

        <div class="results-list">
            @php( $posts = App::get_posts() )

            @if( !empty($posts) )
                @foreach( $posts as $post )
                    @php
                        $preview = get_field( 'preview_image', $post->ID );
                        $cats = wp_get_post_terms( $post->ID, 'module_categories' );

                        // Get the featured image src
                        $featured_img_id = get_post_thumbnail_id( $post->ID );
                        $featured_img_array = wp_get_attachment_image_src( $featured_img_id, 'background', true );
                        $bg_url = '';
                        if ( $featured_img_array ) $bg_url = $featured_img_array[0];

                        $module = [
                            'ID'        => $post->ID,
                            'cat'       => $cats[0]->term_id,
                            'title'     => $post->post_title,
                            'subtitle'  => get_field( 'subtitle', $post->ID ),
                            'permalink' => get_the_permalink( $post->ID ),
                            'content'   => apply_filters('the_content', $post->post_content ),
                            'excerpt'   => wp_trim_words( $post->post_excerpt, 17 ),
                            'image'     => $preview['sizes']['large'],
                            'background'=> $bg_url,
                        ];
                    @endphp

                    @include( 'blocks.module-preview', $module )
                @endforeach
            @endif
        </div>
    </div>
</div>