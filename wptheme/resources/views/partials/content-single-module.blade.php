@foreach( $slides as $slide )

    @if( $slide->layout === 'general_light' || $slide->layout === 'general_dark' )
    <section class="section section-general {{ $slide->layout }}" style="background-image: url({!! $slide->background !!});">
        <div class="background-gradient" style="background: linear-gradient(to right, rgba({{$slide->gradient_color['r']}}, {{$slide->gradient_color['g']}}, {{$slide->gradient_color['b']}}, 1) 0%, rgba({{$slide->gradient_color['r']}}, {{$slide->gradient_color['g']}}, {{$slide->gradient_color['b']}}, 0.7) 53%, rgba({{$slide->gradient_color['r']}}, {{$slide->gradient_color['g']}}, {{$slide->gradient_color['b']}}, 0) 100%);"></div>

        <div class="container">
            <div class="container--inner">
                <header class="section-header">
                    <h2 class="section-title">{!! $slide->title !!}</h2>

                    @if( !empty($slide->subtitle) )
                        <h3 class="section-subtitle">{!! $slide->subtitle !!}</h3>
                    @endif
                </header>

                <div class="slide-content">
                    {!! $slide->content !!}

                    @if( $slide->show_sharing )
                        <div class="page-sharing">
                            <button type="button" data-js="toggle-social">
                                <i class="fas fa-share-alt"></i>
                            </button>

                            <div class="page-sharing--options">
                            {!! do_shortcode('[Sassy_Social_Share]') !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @endif

    @if( $slide->layout === 'section_module' )
    <section class="section section-single-module" style="background-image: url({!! $slide->background !!});">
        <div class="container">
            <div class="container--inner">
                <header class="section-header">
                    <h2 class="section-title">{!! $slide->title !!}</h2>

                    @if( !empty($slide->subtitle) )
                        <h3 class="section-subtitle">{!! $slide->subtitle !!}</h3>
                    @endif
                </header>

                <div class="section-content">
                    {!! $slide->content !!}

                    @if( $slide->show_sharing )
                        <div class="page-sharing">
                            <button type="button" data-js="toggle-social">
                                <i class="fas fa-share-alt"></i>
                            </button>

                            <div class="page-sharing--options">
                            {!! do_shortcode('[Sassy_Social_Share]') !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @endif

    @if( $slide->layout === 'section_video' )
    <section class="section section-video">
        <div class="video-overlay" style="background-image: url({!! $slide->poster !!});">
            <div class="container">
                <div class="container--inner">
                    <button class="button cta-primary btn-play" type="button" data-js="play">{{ $slide->button }}</button>

                    @if( !empty($slide->title) )
                        <strong class="video-title">{{ $slide->title }}</strong>
                        {!! $slide->description !!}
                    @endif
                </div>
            </div>
        </div>

        {!! $slide->video !!}
    </section>
    @endif


    @if( $slide->layout === 'section_hotspots' )
    <section class="section section-hotspots" style="background-image: url({!! $slide->background !!});">
        @if( !empty($slide->hotspots) )
            @foreach( $slide->hotspots as $hotspot )
                <div class="hotspot {{ $hotspot['orientation'] }}" style="top: {!! $hotspot['top'] !!}%; left: {!! $hotspot['left'] !!}%; ">
                    <svg width="36" height="36" viewBox="0 0 36 36" data-js="toggle-hotspot">
                        <rect width="36" height="36" rx="5" ry="5" />
                        <path d="M26.17,20.44H19.85V26.7H16.6V20.44H10.28V17.26H16.6V11h3.25v6.26h6.32v3.18Z" />
                    </svg>

                    <div class="hotspot-info">
                        <div class="hotspot-title">{!! $hotspot['title'] !!}</div>
                        {!! $hotspot['description'] !!}
                    </div>
                </div>
            @endforeach
        @endif

        @if( !empty($slide->hotspots) )
            <div class="container mobile-only">
                <dl class="hotspots-list">
                    @foreach( $slide->hotspots as $hotspot )
                        <div class="hotspot-definition">
                            <dd>
                                <svg width="36" height="36" viewBox="0 0 36 36">
                                    <rect width="36" height="36" rx="5" ry="5" />
                                    <path d="M26.17,20.44H19.85V26.7H16.6V20.44H10.28V17.26H16.6V11h3.25v6.26h6.32v3.18Z" />
                                </svg>

                                <div class="hotspot-title">{!! $hotspot['title'] !!}</div>
                            </dd>

                            <dt class="hotspot-description">
                                <div class="hotspot-description--inner">
                                    {!! $hotspot['description'] !!}
                                </div>
                            </dt>
                        </div>
                    @endforeach
                </dl>
            </div>
        @endif
    </section>
    @endif


    @if( $slide->layout === 'section_split' )
    <section class="section section-split">
        <div class="col-left" style="background-image: url({!! $slide->left['background'] !!});">
            <div class="container">
                <div class="container--inner">
                    <header class="section-header">
                        <h2 class="section-title">{!! $slide->left['title'] !!}</h2>

                        @if( !empty($slide->subtitle) )
                            <h3 class="section-subtitle">{!! $slide->left['subtitle'] !!}</h3>
                        @endif
                    </header>

                    {!! $slide->left['content'] !!}
                </div>
            </div>
        </div>

        <div class="col-right">
            <div class="modules-list">
                @if( $slide->right['next_module'] )
                <div class="module">
                    @php( $module_acf = $slide->right['next_module'] )
                    @php( $module = Modules::get_single($module_acf->ID) )

                    <div class="group-title next">{{ __('Next Module', 'theme') }}</div>
                    @include('blocks.module-preview', $module)
                </div>
                @endif

                @if( $slide->right['previous_module'] )
                <div class="module">
                    @php( $module_acf = $slide->right['previous_module'] )
                    @php( $module = Modules::get_single($module_acf->ID) )

                    <div class="group-title previous">{{ __('Previous Module', 'theme') }}</div>
                    @include('blocks.module-preview', $module)
                </div>
                @endif

                @if( $slide->right['recommended_module'] )
                <div class="module">
                    @php( $module_acf = $slide->right['recommended_module'] )
                    @php( $module = Modules::get_single($module_acf->ID) )

                    <div class="group-title recommended">{{ __('Recommended Module', 'theme') }}</div>
                    @include('blocks.module-preview', $module)
                </div>
                @endif
            </div>

            <div class="call-to-action">
                @foreach( $slide->ctas as $cta )
                    <p class="cta">
                        <a class="button cta-primary" href="{!! $cta['button_url'] !!}">
                            {!! $cta['button_label'] !!}
                        </a>

                        <span class="cta-description desktop-only">{!! $cta['description'] !!}</span>
                    </p>
                @endforeach
            </div>
        </div>
    </section>
    @endif

@endforeach