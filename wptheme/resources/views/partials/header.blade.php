<header class="site-header">
    <div class="container">
        <a class="logo-main" href="{{ home_url('/') }}?src=origin">
            {{ get_bloginfo('name', 'display') }}
        </a>

        @if( is_front_page() )
        <div class="section-search" {!! $show_landing ? 'style="display: none;"' : '' !!}>
            {{ get_search_form() }}

            <div class="social-links">
                @if( !empty($social_links->twitter) )
                <a class="social-link" href="{!! $social_links->twitter !!}" target="_blank" rel="noopener">
                    <i class="fab fa-twitter"></i>
                </a>
                @endif

                @if( !empty($social_links->pinterest) )
                <a class="social-link" href="{!! $social_links->pinterest !!}" target="_blank" rel="noopener">
                    <i class="fab fa-pinterest"></i>
                </a>
                @endif

                @if( !empty($social_links->facebook) )
                <a class="social-link" href="{!! $social_links->facebook !!}" target="_blank" rel="noopener">
                    <i class="fab fa-facebook-f"></i>
                </a>
                @endif
            </div>
        </div>
        @endif

        <button id="navToggle" class="burger" type="button">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>
</header>