@extends('layouts.app')

@section('content')
    @if (!have_posts())
        <div class="container--inner">
            <h1 class="page-title">{{ App::title() }}</h1>
            <p>{{ __('Sorry, but the page you were trying to view does not exist.', 'theme') }}</p>
            <p><a href="<?php echo esc_url(home_url('/')); ?>">{{ __('Back to Homepage', 'theme') }}</a></p>
        </div>
    @endif
@endsection
